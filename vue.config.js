// 通过CompressionWebpackPlugin插件build提供压缩
const CompressionWebpackPlugin = require('compression-webpack-plugin');
const productionGzipExtensions = ['js', 'css'];
const isProduction = process.env.NODE_ENV === 'production';

// 修改uglifyOptions去除console来减少文件大小
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    // 设置打包路劲
    publicPath: './',
    // 使用http-proxy-middleware设置代理
    // devServer: {
    //     proxy: {
    //         '/api': {
    //             // 代理的目的api
    //             target: 'http://192.168.2.192:8080',
    //             changeOrigin: true,
    //             pathRewrite: {
    //                 '^/api': '',
    //             },
    //         },
    //     },
    // },
    outputDir: 'pages',
    configureWebpack: config => {
        if (isProduction) {
            config.plugins.push(
                new CompressionWebpackPlugin({
                    algorithm: 'gzip',
                    test: new RegExp(
                        '\\.(' + productionGzipExtensions.join('|') + ')$',
                    ),
                    threshold: 10240,
                    minRatio: 0.8,
                }),
            );
            config.plugins.push(
                new UglifyJsPlugin({
                    uglifyOptions: {
                        compress: {
                            warnings: false,
                            drop_debugger: true,
                            drop_console: true,
                        },
                    },
                    sourceMap: false,
                    parallel: true,
                }),
            );
        }
    },
};
