import Vue from 'vue';
import App from './App.vue';
import store from './vuex/index';
import router from './router/index';

// import axios from 'axios';
// import VueAxios from 'vue-axios';
// Vue.use(VueAxios, axios);

// 按需引入组件
import { Button, Toast } from 'vant';
Vue.use(Button, Toast);

Vue.config.productionTip = false;

// 设置生产环境和开发环境
if (process.env.NODE_ENV !== 'production') {
    // 开发环境
    console.log('dev!');
}
if (process.env.NODE_ENV === 'production') {
    // 生产环境
    console.log('pro!');
}

new Vue({
    render: h => h(App),
    store,
    router,
}).$mount('#app');
