// 用户模块store
export default {
    state: {
        userInfo: {
            id: 1,
            name: 'chenSir',
        },
    },
    actions: {
        setUserInfo: function({ commit }, data) {
            commit('setUserInfo', data);
        },
    },
    getters: {
        getUserInfo: function(state) {
            return state.userInfo;
        },
    },
    mutations: {
        setUserInfo: function(state, data) {
            state.userInfo = data;
        },
    },
    namespaced: true,
};
