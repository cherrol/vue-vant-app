import Mock from 'mockjs';
const Random = Mock.Random;

// 登录
Mock.mock('/api/user/login', /post|get/i, (option = {}) => {
    // console.log('Mock收到的请求参数:');
    // console.log(option);
    let name = Random.cname();
    return {
        code: 200,
        msg: '登陆成功！',
        data: {
            id: Random.id(),
            name,
            headPortrait: Random.image(
                '500x500',
                '#50B347',
                '#FFF',
                name + '.jpg',
            ),
        },
    };
});
