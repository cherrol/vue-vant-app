// 基础路径
export const BASE_URL = '/api';

// token
export const AUTH_TOKEN = sessionStorage.getItem('token');
