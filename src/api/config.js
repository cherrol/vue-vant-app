import axios from 'axios';
import { Toast } from 'vant';
import { BASE_URL, AUTH_TOKEN } from '../common/base-config';
import '../mock/mock';

axios.defaults.baseURL = BASE_URL;

axios.defaults.timeout = 5000;
if (AUTH_TOKEN) {
    axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
}
axios.defaults.headers.post['Content-Type'] =
    'application/x-www-form-urlencoded';

// 添加请求拦截器
axios.interceptors.request.use(
    function(config) {
        console.log('\nRequest:');
        console.log(config.url);
        console.log(config.data);

        // 在发送请求之前做些什么

        return config;
    },
    function(error) {
        // 对请求错误做些什么
        return Promise.reject(error);
    },
);

// 响应拦截器
axios.interceptors.response.use(
    response => {
        console.log('Response:');
        console.log(response.status);
        console.log(response.data || response.data);

        // 如果返回的状态码为200，说明接口请求成功，可以正常拿到数据
        // 否则的话抛出错误
        if (response.status === 200) {
            return Promise.resolve(response);
        } else {
            return Promise.reject(response);
        }
    },
    // 服务器状态码不是2开头的的情况
    // 这里可以跟你们的后台开发人员协商好统一的错误状态码
    // 然后根据返回的状态码进行一些操作，例如登录过期提示，错误提示等等
    // 下面列举几个常见的操作，其他需求可自行扩展
    error => {
        if (error.response.status) {
            switch (error.response.status) {
                // 401: 未登录
                // 未登录则跳转登录页面，并携带当前页面的路径
                // 在登录成功后返回当前页面，这一步需要在登录页操作。
                case 401:
                    break;

                // 403 token过期
                // 登录过期对用户进行提示
                // 清除本地token和清空vuex中token对象
                // 跳转登录页面
                case 403:
                    break;

                // 404请求不存在
                case 404:
                    break;
                // 其他错误，直接抛出错误提示
                default:
                    Toast({
                        message: error.response.data.message,
                        duration: 1500,
                        forbidClick: true,
                    });
            }
            return Promise.reject(error.response);
        }
    },
);

export default axios;
