// 接口处理
import { fetch, post } from './http';

export const login = function(param = {}) {
    return post('user/login', param);
};

export const register = (param = {}) => {
    return post('/user/register', param);
};
