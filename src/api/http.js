import axios from './config';
import qs from 'qs';

/**
 * 封装get方法
 * @param url
 * @param data
 * @returns {Promise}
 */

export function fetch(url, params = {}) {
    return new Promise((resolve, reject) => {
        axios
            .get(url, params)
            .then(response => {
                resolve(response.data || response.rows);
            })
            .catch(err => {
                reject(err);
            });
    });
}

/**
 * 封装post请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function post(url, data = {}) {
    return new Promise((resolve, reject) => {
        axios.post(url, qs.stringify(data)).then(
            response => {
                resolve(response.data || response.rows);
            },
            err => {
                reject(err);
            },
        );
    });
}
