import Router from 'vue-router';
import Vue from 'vue';

Vue.use(Router);

export default new Router({
    routes: [
        {
            name: 'home',
            path: '/',
            // 路由懒加载
            component: () => import('../views/home.vue'),
            // 路由守卫 https://router.vuejs.org/zh/guide/advanced/navigation-guards.html#%E5%85%A8%E5%B1%80%E5%89%8D%E7%BD%AE%E5%AE%88%E5%8D%AB
            // beforeEnter: (to, from, next) => {
            //     // todo
            // },
        },
    ],
});
